FROM registry.gitlab.com/verademo/tomcat

ADD target/demo-*.war /usr/share/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh", "run"]